$(document).ready(function() {
    var search;
    $("#tags").on("input", function() {    
        if($("#tags").val().length >= 3 && $("#tags").val().length <= 5)
        {
            $searchValue = $("#tags").val();
            $.ajax({
                type:'GET',
                url:'https://api.themoviedb.org/3/search/multi?api_key=beb0c664b4b2eee983a943078de43bdd&page=1&include_adult=false&query='+$searchValue,
                success:function(result){
                    var filtered = result.results.filter(function(item){
                        return (item.media_type == "movie" || item.media_type == "person" );         
                    });

                    filtered = $.map(filtered, function (value, key) {
                        $("#searchtype").val("actormovie");
                        if(value.title)
                        {
                            return {
                                value: value.title,
                                id: value.id
                            }
                        }
                        else
                        {
                            return {
                                value: value.name,
                                id: value.id
                            }
                        }
                    })
                    search = filtered;
                }
            });
        }

        $( "#tags" ).autocomplete({            
            source: function (request, response) {                                                 
                response(search.slice(0,5));                 
            }
        });
    });
});
