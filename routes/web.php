<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Security
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Home
Route::get('/', 'HomeController@index');

//FAQ
Route::view('/faq', 'faq');

//Donate
Route::view('/donate', 'donate');

//Contact
Route::view('/contact', 'contact');

//Blog
Route::view('/blog', 'blog');

//Wilt
Route::post('/wilt', 'wilt@wilt');

//Search
Route::get('/search/{searchtype}/{page}/{search}', 'search@search');
Route::post('/search/{page}', 'search@index');

//Reviews
Route::get('/reviews', 'reviews@index');

//Movie Lists
Route::get('/popular/{page}', 'movies@popular');
Route::get('/top/{page}', 'movies@top');
Route::get('/upcoming/{page}', 'movies@upcoming');
Route::get('/playing/{page}', 'movies@playing');

//Movie
Route::get('/movie/{id}/{page}', 'movie@index');
Route::post('/movie/{id}/{page}', 'movie@store');
Route::patch('/movie/{id}/{page}', 'movie@update');
Route::delete('/movie/{id}/{page}', 'movie@destroy');

//User
Route::get('/user/{id}', 'userpage@index');
Route::post('/user/{id}', 'userpage@store');
Route::patch('/user/{id}', 'userpage@update');
Route::delete('/user/{id}', 'userpage@destroy');

//Actor
Route::get('/actor/{id}', 'actorpage@index');
Route::post('/actor/{id}', 'actorpage@store');
Route::patch('/actor/{id}', 'actorpage@update');
Route::delete('/actor/{id}', 'actorpage@destroy');

//Company
Route::get('/company/{id}', 'companypage@index');
Route::post('/company/{id}', 'companypage@store');
Route::patch('/company/{id}', 'companypage@update');
Route::delete('/company/{id}', 'companypage@destroy');