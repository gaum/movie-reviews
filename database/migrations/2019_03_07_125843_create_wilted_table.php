<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWiltedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wilted', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('reviewID');
            $table->integer('userID');
            $table->integer('wiltedType');
            $table->integer('wiltedness');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wilted');
    }
}
