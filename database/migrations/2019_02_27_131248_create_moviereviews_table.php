<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviereviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moviereviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('movieID');
            $table->integer('userID');
            $table->integer('score');
            $table->string('title')->nullable();
            $table->text('review')->nullable();
            $table->string('movieName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moviereviews');
    }
}
