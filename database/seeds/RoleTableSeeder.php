<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $role_manager = new Role();
        $role_manager->name = 'Administrator';
        $role_manager->description = 'Control Account';
        $role_manager->save();

        //Moderator
        $role_manager = new Role();
        $role_manager->name = 'Moderator';
        $role_manager->description = 'Moderating Account';
        $role_manager->save();

        //Seedling
        $role_manager = new Role();
        $role_manager->name = 'Seedling';
        $role_manager->description = 'Newest User';
        $role_manager->save();

        //Sprout
        $role_manager = new Role();
        $role_manager->name = 'Sprout';
        $role_manager->description = 'Intermediate User';
        $role_manager->save();

        //Lettuce Head
        $role_manager = new Role();
        $role_manager->name = 'Lettuce Head';
        $role_manager->description = 'Advanced User';
        $role_manager->save();
    }
}
