<?php

use Illuminate\Database\Seeder;
use App\WiltedTypes;

class WiltedTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wilted = new WiltedTypes();
        $wilted->wiltedID = 1;
        $wilted->name = "I don't like it";
        $wilted->save();

        $wilted = new WiltedTypes();
        $wilted->wiltedID = 2;
        $wilted->name = "Spam";
        $wilted->save();

        $wilted = new WiltedTypes();
        $wilted->wiltedID = 3;
        $wilted->name = "Hateful or abusive";
        $wilted->save();

        $wilted = new WiltedTypes();
        $wilted->wiltedID = 4;
        $wilted->name = "Sexual content";
        $wilted->save();

        $wilted = new WiltedTypes();
        $wilted->wiltedID = 5;
        $wilted->name = "Harmful dangerous acts";
        $wilted->save();

        $wilted = new WiltedTypes();
        $wilted->wiltedID = 6;
        $wilted->name = "Other";
        $wilted->save();
    }
}
