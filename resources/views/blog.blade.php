@extends('layouts.layout')

@section('title')
    Blog
@endsection

@section('content')

    <h5 class="text-center style">Lettuce News and Updates</h5>

    <br>
    <h6 class="font-weight-bold style">Request Limiting</h6>
    <p><span class="badge badge-info">Friday, March 8, 2019</span></p>
    <p>Hey everyone! This morning I bought some ad time on various websites. If I'm lucky, this will bring in some more reviewers. I finally feel as though the site is
    ready and good to go as far as functionality goes.</p>
    <p>As you may now, this website is driven by The Movie Database. They have a huge amount of data on almost half a million movies, actors, and more. TMDb implements Request
    rate limiting. This means that a maximum of 40 requests every 10 seconds can be submitted per IP address. Right now, everything but the search function uses the web server
    to do API calls. This means if 40 people try to use the site at the same time, they may run into that request limitation.</p>
    <p>To combat this, I will be switching often used pages over to AJAX. This will both take a bit of load off the web server, but also increase the amount of 
    traffic we can support! I know we are nowhere near 40 requests per 10 seconds, but I'd rather be prepared.</p>
    <p>These changes will take some time and thought, so I will be implementing it over the next few weeks.</p>

    <br>

    <hr>

    <br>
    <h6 class="font-weight-bold style">Wilted Lettuce</h6>
    <p><span class="badge badge-info">Thursday, March 7, 2019</span></p>
    <p>Hi all. Today I've added in the 'Wilted Review' feature. This feature was put in place to both help moderate content, and help you filter out
    reviews that you do not like.</p>
    <p>
        <button class="btn btn-success" style="background-color:olive" title="I don't like this" type="button"><i class="fas fa-leaf fa-rotate-90"></i></button>
    </p>
    <p>You will see a new leaf icon on the top-right of movie reviews. Clicking this icon will bring up a modal that asks what your reason is for disliking 
    this review. Once you hit submit, the 'wilted level' of the review will increase. For regular users, the level will increase by 1. For moderators, this
    increase is by 5, while for administrators, this increase is by 10.</p>
    <p>Next, you will see a new option under your profile page. This option allows you to set the wilted review tolerance level. This level can be anything 
    above zero. When you set this tolerance level, any review with a wilted leavel higher than this number, will not have their review title and text displayed.</p>
    <p>The way that I envisioned this feature to work is as such: I see a review that is either spam, hateful, or simply rubs me the wrong way. I click
    the wilted review button and hit submit after selecting my reason. If my wilted tolerance level is set to 10, and 9 other people do this, 
    that review will no longer be visible to anyone who has their tolerance level set to 10. I can always go back and change my tolerance level to lower than 10.</p>
    <p>The default tolerance level for the public and new users will be set to 10.</p>
    <p>Feel free to contact me about any concerns or errors you may find!</p>
    <p>Also, we have a new temporary logo!</p>
    <br>

    <hr>

    <br>
    <h6 class="font-weight-bold style">Newest Features</h6>
    <p><span class="badge badge-info">Wednesday, March 6, 2019</span></p>
    <p>These new features were just added today!</p>
    <ul>
        <li>reCAPTCHA integration (for your, and my protection)!</li>
        <li>Predictive search (still a tad buggy)</li>
        <li>Reviewer tabs. Now you can see all your own reviews and interests. Other users cannot see this.</li>
    </ul>
    <br>

@endsection