@extends('layouts.layout')

@section('title')
    Frequently Asked Questions
@endsection

@section('content')

    <h5 class="text-center style">Frequently Asked Questions</h5>
    <br>
    <h6 class="font-weight-bold style">Where do you get your movie information?</h6>
    <p>This product uses the TMDb API but is not endorsed or certified by TMDb. Their wonderful API contains a database of almost half a million movies!</p>
    <br>
    <h6 class="font-weight-bold style">Who are you and what do you do?</h6>
    <p>I am a long time web developer working in West Virginia with around 9 years of development experience behind my back. I currently work full time
        as a developer and love every moment of my full time job.
    </p>
    <br>
    <h6 class="font-weight-bold style">What is the privilege level on my profile?</h6>
    <p>Right now, the privilege level does not do anything. In the future I may decide to increase privilege levels for users
        who have reviewed many movies and received many ratings. The end goal will be to allow you to filter movie reviews by privilege level.
    </p>
    <br>

@endsection