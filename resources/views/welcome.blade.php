@extends('layouts.layout')

@section('title')
    Lettuce
@endsection

@section('content')
    <h1 class="display-2 text-center">
        <i class="fas fa-spa text-info"></i>
    </h1>

    <h1 class="text-center title m-b-md style mt-4">
        Let-Us Review
    </h1>

    <blockquote class="blockquote text-center text-secondary font-italic font-weight-light">
        <p class="mb-0"><span style="font-size:200%; max-height:300px">&ldquo;&nbsp;</span>A simple movie review application.<span style="font-size:200%; position:relative; top:0.8em">&nbsp;&rdquo;</span></p>
    </blockquote>

    <div class="alert alert-info" role="alert">
        We have new updates! Head over <a href="/blog" class="alert-link">to the blog</a> to see what's new.
    </div>

    <div class="row">
        <div class="col-12 col-md-6" style="margin:0 auto">
            <div id="carouselExampleIndicators" class="carousel slide w-100" data-ride="carousel">
                <div class="carousel-inner" style="max-height:960px;">
                    <div v-for="(movie, index) in popular.slice(0,10)" :class="{ 'active': index === 0 }" class="carousel-item">
                        <a :href="'/movie/'+ movie.id +'/1'"><img class="d-block w-100" :src="'https://image.tmdb.org/t/p/original/'+ movie.backdrop_path" :alt="movie.title"></a>
                        <div class="carousel-caption d-none d-md-block" style="background-color:rgba(0, 0, 0, 0.5); width:100%; left:0">
                            <h5>@{{ movie.title }}</h5>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12 col-md-6 border-right pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style pt-1 pb-1"><a class="font-weight-bold text-light" href="{{ url('/upcoming/1') }}">Upcoming</a></h3>
            </div>
            <div class="row">
                <div v-for="movie in upcoming.slice(0,6)" class="col-6 col-md-4">                         
                    <a :href="'/movie/'+ movie.id +'/1'"><img class="p-1" style="width:100%" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title"></a>
                </div>
            </div>
            <div class="p-1">
                <a class="btn btn-sm btn-danger btn-block" href="/upcoming/1"><i class="fas fa-plus-square"></i>&nbsp;More...</a>
            </div>
        </div>
        <div class="col-12 col-md-6 border-left pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style pt-1 pb-1"><a class="font-weight-bold text-light" href="{{ url('/playing/1') }}">Now Playing</a></h3>
            </div>
            <div class="row">
                <div v-for="movie in playing.slice(0,6)" class="col-6 col-md-4">                         
                    <a :href="'/movie/'+ movie.id +'/1'"><img class="p-1" style="width:100%" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title"></a>
                </div>
            </div>
            <div class="p-1">
                <a class="btn btn-sm btn-danger btn-block" href="/playing/1"><i class="fas fa-plus-square"></i>&nbsp;More...</a>
            </div>
        </div>
    </div>

    <hr>

    <div class="row mb-2">
        <div class="col-12 col-md-6 border-right pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style pt-1 pb-1"><a class="font-weight-bold text-light" href="{{ url('/popular/1') }}">Popular</a><h3>
            </div>
            <div class="row">   
                <div v-for="movie in popular.slice(0,6)" class="col-6 col-md-4">                         
                    <a :href="'/movie/'+ movie.id +'/1'"><img class="p-1" style="width:100%" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title"></a>
                </div>
            </div>
            <div class="p-1">
                <a class="btn btn-sm btn-danger btn-block" href="/popular/1"><i class="fas fa-plus-square"></i>&nbsp;More...</a>
            </div>
        </div>
        <div class="col-12 col-md-6 border-left pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style pt-1 pb-1"><a class="font-weight-bold text-light" href="{{ url('/top/1') }}">Top Rated</a></h3>
            </div>
            <div class="row">
                <div v-for="movie in top.slice(0,6)" class="col-6 col-md-4">                         
                    <a :href="'/movie/'+ movie.id +'/1'"><img class="p-1" style="width:100%" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title"></a>
                </div>
            </div>
            <div class="p-1">
                <a class="btn btn-sm btn-danger btn-block" href="/top/1"><i class="fas fa-plus-square"></i>&nbsp;More...</a>
            </div>
        </div>
    </div>
                
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    popular: null,
                    upcoming: null,
                    top: null,
                    playing: null
                }
            },
            mounted () {
                //Popular
                axios
                .get('https://api.themoviedb.org/3/movie/popular?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page=1')
                .then(response => (this.popular = response.data.results))
                
                //Upcoming
                axios
                .get('https://api.themoviedb.org/3/movie/upcoming?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page=1')
                .then(response => (this.upcoming = response.data.results))

                //Top Rated
                axios
                .get('https://api.themoviedb.org/3/movie/top_rated?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page=1')
                .then(response => (this.top = response.data.results))

                //Now Playing
                axios
                .get('https://api.themoviedb.org/3/movie/now_playing?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page=1')
                .then(response => (this.playing = response.data.results))
            }
        })
    </script>
@endsection