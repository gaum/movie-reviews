@extends('layouts.layout')

@section('title')
    Popular Movies
@endsection

@section('content')
    <div v-for="movie in movies.results" class="row pb-2">
        <div class="col-5 col-md-3">
            <a :href="'/movie/'+ movie.id +'/1'"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title"></a>
        </div>
        <div class="col">
            <h6 class="card-title"><strong>@{{ movie.title }}</strong></h6>
            <p><span class="badge badge-primary">Release Date: @{{ movie.release_date }}</span></p>
            <p class="card-text">@{{ movie.overview }}</p>
            <a class="btn btn-success" :href="'/movie/'+ movie.id +'/1'" role="button">More Info</a>
        </div>
    </div>
    <hr>

    <ul v-if="movies.total_pages > 20" class="pagination">
        @if($page == 1)
            <li class="page-item disabled">
                <span class="page-link">Previous Page</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="/{{ $searchType }}/{{ $page - 1 }}">Previous Page</a>
            </li>
        @endif
        @if($page < 3)
            @if($page - 2 > 0)
                <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page - 2 }}">{{ $page - 2 }}</a></li>
            @endif
            @if($page - 1 > 0)
                <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page - 1 }}">{{ $page - 1 }}</a></li>
            @endif
            <li class="page-item active"><a class="page-link" href="/{{ $searchType }}/{{ $page }}">{{ $page }}</a></li>
                
            <li v-if="{{$page + 2}} < movies.total_pages" class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page + 1 }}">{{ $page + 1 }}</a></li>

            <li v-if="{{$page + 1}} < movies.total_pages" class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page + 2 }}">{{ $page + 2 }}</a></li>

        @else
            <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page - 2 }}">{{ $page - 2 }}</a></li>
            <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page - 1 }}">{{ $page - 1 }}</a></li>
            <li class="page-item active"><a class="page-link" href="/{{ $searchType }}/{{ $page }}">{{ $page }}</a></li>
            <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page + 1 }}">{{ $page + 1 }}</a></li>
            <li class="page-item"><a class="page-link" href="/{{ $searchType }}/{{ $page + 2 }}">{{ $page + 2 }}</a></li>
        @endif

        <li v-if="{{$page}} == movies.total_pages" class="page-item disabled">
            <span class="page-link">Next Page</span>
        </li>
        <li v-else class="page-item">
            <a class="page-link" href="/{{ $searchType }}/{{ $page + 1 }}">Next Page</a>
        </li>
    </ul>
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    movies: null,
                }
            },
            mounted () {
                if("{{ $searchType }}" == 'popular')
                {
                    //Popular
                    axios
                    .get('https://api.themoviedb.org/3/movie/popular?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page='+ {{ $page }})
                    .then(response => (this.movies = response.data))
                }
                else if("{{ $searchType }}" == 'top')
                {
                    //Top
                    axios
                    .get('https://api.themoviedb.org/3/movie/top_rated?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page='+ {{ $page }})
                    .then(response => (this.movies = response.data))
                }
                else if("{{ $searchType }}" == 'upcoming')
                {
                    //Upcoming
                    axios
                    .get('https://api.themoviedb.org/3/movie/upcoming?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page='+ {{ $page }})
                    .then(response => (this.movies = response.data))
                }
                else if("{{ $searchType }}" == 'playing')
                {
                    //Now Playing
                    axios
                    .get('https://api.themoviedb.org/3/movie/now_playing?region=US&api_key=beb0c664b4b2eee983a943078de43bdd&page='+ {{ $page }})
                    .then(response => (this.movies = response.data))
                }

            }
        })
    </script>
@endsection