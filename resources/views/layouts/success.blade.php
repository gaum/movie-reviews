<!--Validation error handling coming from the controller-->
@if(session('success'))
<div class="alert alert-success alert-dismissible pb-0">
    <ul>
        {{ session('success') }}
    </ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif