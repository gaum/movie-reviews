<!--Validation error handling coming from the controller-->
@if(count($errors))
<div class="alert alert-danger alert-dismissible pb-0">
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif