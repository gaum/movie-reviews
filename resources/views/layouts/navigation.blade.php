<nav class="navbar navbar-expand-xl navbar-light bg py-4">
<a class="navbar-brand" href="/">
    <i class="fas fa-spa text-info"></i>
    Let-Us Review
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">Home</a>
        </li>
        <li>
            <div class="dropdown show">
            <a class="btn btn-link text-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Movies
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="nav-link" href="{{ url('/popular/1') }}">Popular</a>
                <a class="nav-link" href="{{ url('/top/1') }}">Top Rated</a>
                <a class="nav-link" href="{{ url('/upcoming/1') }}">Upcoming</a>
                <a class="nav-link" href="{{ url('/playing/1') }}">Now Playing</a>
            </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/reviews') }}">Reviews</a>
        </li>
    </ul>
        @if (Route::has('login'))
            <ul class="navbar-nav float-right">
                <li class="nav-item border-left">
                    <a class="nav-link" href="{{ url('/faq') }}">FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/donate') }}">Donate</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/blog') }}">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                </li>
            @auth
                <li class="nav-item border-left">
                    <a class="nav-link" href="{{ url('/user/'.Auth::user()->id) }}">Profile</a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                </li>
            @else
                <li class="nav-item border-left">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item border-right">
                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                    </li>
                @endif
            @endauth
            </ul>
        @endif   
</div>
</nav>
<div class="row">   
    <div class="col">
        <form method="POST" action="/search/1" class="form" autocomplete="off">
            <div class="form-row">
                {{ csrf_field() }}
                
                <div class="col-12 col-md-8 pt-1 ui-widget">
                    <input id="tags" name="search" class="form-control" type="search" placeholder="Search" aria-label="Search" value="{{ old('search') }}" required>
                </div>
                <div class="col-6 col-md-2 pt-1">
                <select id="searchtype" name="searchtype" class="custom-select">
                    <option value="movie" selected>Movies</option>
                    <option value="actor">Actors</option>
                    <option value="user">Users</option>
                    <option value="company">Companies</option>
                    <option value="actormovie">Movies and Actors</option>
                </select>
                </div>
                <div class="col-6 col-md-2">
                    <button class="btn btn-outline-success btn-block mt-1" type="submit">Search</button>
                </div>
            </div>
        </form> 
    </div>
</div>