<!--This is the layout of all your pages. It contains parts that can be populated by other pages, such as 
    navigation, footers, content areas, etc.-->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="Lettuce Reviews allows reviewers to rate, review, and look up information about movies.">
        <meta name="keywords" content="Movie Review,Movie,Review,Ratings,LettuceReview,Let Us Review,LettuceReviews,Popular,Upcoming,Playing,Top">

        <!--This token is often required for AJAX and forms.-->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!--Yield waits on content from a view. Page-specific titles will be displayed here-->
        <title>@yield('title')</title>

        <!--Bootstrap 4-->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <!--Fontawesome Icons-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!--Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Laila" rel="stylesheet">
        <!--Other Styles-->
        <link rel="stylesheet" href="/css/stars.css">
        <link rel="stylesheet" href="/css/extra.css">
        <link rel="stylesheet" href="/css/jquery-ui.min.css">
        
    </head>
    <body>
        <div class="container">
            <!--Include includes a php page directly into this section-->
            @include('layouts.navigation')

            <br>

            @include('layouts.errors')

            @include('layouts.success')
            
            <div id="app">
                @yield('content')
            </div>

            <br>

            @include('layouts.footer')
        </div>

        <!--Be aware that the default Bootstrap uses the jquery slim build which does not include AJAX-->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/search.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        @yield('specificJS')
    </body>
</html>