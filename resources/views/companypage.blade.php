@extends('layouts.layout')

@section('title')
    Company
@endsection

@section('content')
    <div class="row pb-2">
        <div class="col-12 col-md-3">
            <img class="img-thumbnail mb-2" :src="'https://image.tmdb.org/t/p/w500/'+ company.logo_path" :alt="company.name">

            <!--Interest already exists-->
            @auth
                @if ($interest)
                <div>
                    <form method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <div class="btn-group d-flex" role="group">
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg {{ ($interest->interest=="1")? "active" : "" }}"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit" name="interest" value="0" class="btn btn-outline-danger btn-lg {{ ($interest->interest=="0")? "active" : "" }}"><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @else
                <!--Interest noes not exist-->
                <div class="text-center">
                    <strong>Do you enjoy this company's work?</strong>
                    <form method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="companyName" :value="company.name">

                        <div class="form-group">
                            <div class="btn-group d-flex" role="group" style=>
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg" title="I like them!"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit"  name="interest" value="0" class="btn btn-outline-danger btn-lg" title="I'm not a fan.'"><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            @else
                <div class="alert alert-info pb-0">
                    <p>Please log in to leave a company review.</p>
                </div>
            @endauth

        </div>
        <div class="col">
            <h5 class="style card-title font-weight-bold">@{{ company.name }}</h5>
            <p><span class="badge badge-info">Rating: {{ $score }}%</span> ({{ $total }} votes)</p>
            <p><strong>Homepage:</strong> <a :href="company.homepage" target="_blank">@{{ company.homepage }}</a></p>
            <p><strong>Headquarters:</strong> @{{ company.headquarters }}</p>   
        </div>
    </div>
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    company: null,
                }
            },
            mounted () {
                //Company
                axios
                .get('https://api.themoviedb.org/3/company/'+ {{ $id }} +'?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.company = response.data)).then(response => (document.title = this.company.name))
            }
        })
    </script>
@endsection