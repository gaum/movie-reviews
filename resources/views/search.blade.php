@extends('layouts.layout')

@section('title')
    {{ ucwords($searchtype) }} Search Results
@endsection

@section('content')
    <div v-if="results">
        @if($searchtype == 'movie')
            <div v-for="result in results.results" class="row pb-2">
                <div class="col-5 col-md-3">
                    <a :href="'/movie/'+ result.id +'/1'"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ result.poster_path" alt="result.title"></a>
                </div>
                <div class="col">
                    <h6 class="card-title">@{{ result.title }}</h6>
                    <p><span class="badge badge-primary">Release Date: @{{ result.release_date }}</span></p>
                    <p class="card-text">@{{ result.overview }}</p>
                    <a class="btn btn-success" :href="'/{{ $searchtype }}/'+ result.id +'/1'" role="button">More Info</a>
                </div>
            </div>
        @elseif($searchtype == 'actor')
            <div v-for="result in results.results" class="row pb-2">
                <div class="col-5 col-md-3">
                    <a :href="'/actor/'+ result.id"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ result.profile_path" alt="result.name"></a>
                </div>
                <div class="col">
                    <h6 class="card-title">@{{ result.name }}</h6>
                    <a class="btn btn-success" :href="'/{{ $searchtype }}/'+ result.id" role="button">Biography</a>
                </div>
            </div>
        @elseif($searchtype == 'actormovie')
                <div v-for="result in results.results">
                    <!--Movie-->
                    <div v-if="result.media_type == 'movie'" class="row pb-2">
                        <div class="col-5 col-md-3">
                            <a :href="'/movie/'+ result.id +'/1'"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ result.poster_path" alt="result.title"></a>
                        </div>
                        <div class="col">
                            <h6 class="card-title">@{{ result.title }}</h6>
                            <p><span class="badge badge-primary">Release Date: @{{ result.release_date }}</span></p>
                            <p class="card-text">@{{ result.overview }}</p>
                            <a class="btn btn-success" :href="'/movie/'+ result.id +'/1'" role="button">More Info</a>
                        </div>
                    </div>
                    <!--Actor-->
                    <div v-if="result.media_type == 'person'" class="row pb-2">
                        <div class="col-5 col-md-3">
                            <a :href="'/actor/'+ result.id"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ result.profile_path" alt="result.name"></a>
                        </div>
                        <div class="col">
                            <h6 class="card-title">@{{ result.name }}</h6>
                            <a class="btn btn-success" :href="'/actor/'+ result.id" role="button">Biography</a>
                        </div>
                    </div>
                </div>
        @elseif($searchtype == 'company')
            <div v-for="result in results.results" class="row pb-2">
                <div class="col-5 col-md-3">
                    <a :href="'/company/'+ result.id"><img class="img-thumbnail" :src="'https://image.tmdb.org/t/p/w500/'+ result.logo_path" alt="result.name"></a>
                </div>
                <div class="col">
                    <h6 class="card-title">@{{ result.name }}</h6>
                    <a class="btn btn-success" :href="'/{{ $searchtype }}/'+ result.id" role="button">More Info</a>
                </div>
            </div>
        @endif

        <!--Pagination-->
        <ul v-if="results.total_pages > 20" class="pagination">
            @if($page == 1)
                <li class="page-item disabled">
                    <span class="page-link">Previous Page</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="/search/{{ $searchtype }}/{{ $page - 1 }}/{{ $searchvalue }}">Previous Page</a>
                </li>
            @endif
            @if($page < 3)
                @if($page - 2 > 0)
                    <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page - 2 }}/{{ $searchvalue }}">{{ $page - 2 }}</a></li>
                @endif
                @if($page - 1 > 0)
                    <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page - 1 }}/{{ $searchvalue }}">{{ $page - 1 }}</a></li>
                @endif
                <li class="page-item active"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page }}/{{ $searchvalue }}">{{ $page }}</a></li>
                
                <li v-if="{{$page + 2}} < results.total_pages" class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page + 1 }}/{{ $searchvalue }}">{{ $page + 1 }}</a></li>

                <li v-if="{{$page + 1}} < results.total_pages" class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page + 2 }}/{{ $searchvalue }}">{{ $page + 2 }}</a></li>

            @else
                <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page - 2 }}/{{ $searchvalue }}">{{ $page - 2 }}</a></li>
                <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page - 1 }}/{{ $searchvalue }}">{{ $page - 1 }}</a></li>
                <li class="page-item active"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page }}">{{ $page }}</a></li>
                <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page + 1 }}/{{ $searchvalue }}">{{ $page + 1 }}</a></li>
                <li class="page-item"><a class="page-link" href="/search/{{ $searchtype }}/{{ $page + 2 }}/{{ $searchvalue }}">{{ $page + 2 }}</a></li>
            @endif

                <li v-if="{{$page}} == results.total_pages" class="page-item disabled">
                    <span class="page-link">Next Page</span>
                </li>

                <li v-else class="page-item">
                    <a class="page-link" href="/search/{{ $searchtype }}/{{ $page + 1 }}/{{ $searchvalue }}">Next Page</a>
                </li>
        </ul>
    </div>

    <div v-else class="row pb-2">
        <div class="col">
            <h3 class="card-title text-center">No Results</h3>
        </div>
    </div>

        @if($searchtype == 'user')
            @if(!$results->isEmpty())
                <h5>Top 20 Results</h5>
                @foreach ($results as $result)
                    <div class="row pb-2">
                        <div class="col border rounded p-2 m-2">
                            <h6 class="card-title">{{ $result->name }}</h6>
                            <p><span class="badge badge-primary">Privilege Level: {{ $result->role }}</span></p>
                            <a href="{{ $result->website }}" target="_blank" class="card-link">{{ $result->website }}</a>
                            <p class="card-text">{{ $result->bio }}</p>
                            <a class="btn btn-success" href="/{{ $searchtype }}/{{ $result->id }}" role="button">More Info</a>
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    results: null,
                }
            },
            mounted () {
                if("{{ $searchtype }}" == 'movie')
                {
                    //Popular
                    axios
                    .get('https://api.themoviedb.org/3/search/movie?api_key=beb0c664b4b2eee983a943078de43bdd&page=1&include_adult=false&query='+ '{{$searchvalue}}'+ '&page='+ {{$page}})
                    .then(response => (this.results = response.data))
                }
                else if("{{ $searchtype }}" == 'actor')
                {
                    //Top
                    axios
                    .get('https://api.themoviedb.org/3/search/person?api_key=beb0c664b4b2eee983a943078de43bdd&page=1&include_adult=false&query='+ '{{$searchvalue}}'+ '&page='+ {{$page}})
                    .then(response => (this.results = response.data))
                }
                else if("{{ $searchtype }}" == 'actormovie')
                {
                    //Top
                    axios
                    .get('https://api.themoviedb.org/3/search/multi?api_key=beb0c664b4b2eee983a943078de43bdd&page=1&include_adult=false&query='+ '{{$searchvalue}}'+ '&page='+ {{$page}})
                    .then(response => (this.results = response.data))
                }
                else if("{{ $searchtype }}" == 'company')
                {
                    //Upcoming
                    axios
                    .get('https://api.themoviedb.org/3/search/company?api_key=beb0c664b4b2eee983a943078de43bdd&page=1&query='+ '{{$searchvalue}}'+ '&page='+ {{$page}})
                    .then(response => (this.results = response.data))
                }
            }
        })
    </script>
@endsection