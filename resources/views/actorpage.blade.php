@extends('layouts.layout')

@section('title')
    Actor
@endsection

@section('content')
    <div class="row pb-2">
        <div class="col-12 col-md-3">
            <img class="img-thumbnail mb-2" :src="'https://image.tmdb.org/t/p/w500/'+ actor.profile_path" :alt="actor.name">

            <!--Interest already exists-->
            @auth
                @if ($interest)
                <div>
                    <form method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <div class="btn-group d-flex" role="group">
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg {{ ($interest->interest=="1")? "active" : "" }}"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit" name="interest" value="0" class="btn btn-outline-danger btn-lg {{ ($interest->interest=="0")? "active" : "" }}"><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @else
                <!--Interest noes not exist-->
                <div class="text-center">
                    <strong>Do you enjoy this actor's work?</strong>
                    <form method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="actorName" :value="actor.name">

                        <div class="form-group">
                            <div class="btn-group d-flex" role="group" style=>
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg" title="I like this actor!"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit"  name="interest" value="0" class="btn btn-outline-danger btn-lg" title="I'm not a fan."><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            @else
                <div class="alert alert-info pb-0">
                    <p>Please log in to leave an actor review.</p>
                </div>
            @endauth

        </div>
        <div class="col">
            <h5 class="style card-title font-weight-bold">@{{ actor.name }}</h5>
            <p><span class="badge badge-info">Rating: {{ $score }}%</span> ({{ $total }} votes)</p>
            <p><strong>Date of Birth:</strong> @{{ actor.birthday }}</p>
            <p class="card-text">@{{ actor.biography }}</p>  

            <p class="font-weight-bold pt-3">Photos:</p>
            <div class="row">
                <div v-for="image in images.slice(0,30)" class="col-4 col-md-2 pb-2">
                    <a :href="'https://image.tmdb.org/t/p/w500/'+ image.file_path" target="_blank"><img :src="'https://image.tmdb.org/t/p/w500/'+ image.file_path" width="100%"/></a>
                </div>
            </div>  

            <p class="font-weight-bold pt-3">Cast for movies:</p>
            <div class="row">
                <div class="col">
                    <span v-for="movie in credits.cast.slice(0,30)">@{{ movie.title }} as @{{ movie.character }},&nbsp;</span>
                </div>
            </div>    

            <p class="font-weight-bold pt-3">Crew for movies::</p>
            <div class="row">
                <div class="col">
                    <span v-for="movie in credits.crew.slice(0,30)">@{{ movie.title }} as @{{ movie.job }},&nbsp;</span>
                </div>
            </div>     
        </div>
    </div>
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    actor: null,
                    images: null,
                    credits: null,
                }
            },
            mounted () {
                //Actor
                axios
                .get('https://api.themoviedb.org/3/person/'+ {{$id}} +'?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.actor = response.data)).then(response => (document.title = this.actor.name))
                
                //Images
                axios
                .get('https://api.themoviedb.org/3/person/'+ {{$id}} +'/images?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.images = response.data.profiles))

                //Credits
                axios
                .get('https://api.themoviedb.org/3/person/'+ {{$id}} +'/movie_credits?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.credits = response.data))
            }
        })
    </script>
@endsection