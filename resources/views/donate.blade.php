@extends('layouts.layout')

@section('title')
    Donate
@endsection

@section('content')

    <h5 class="text-center style">Donate</h5>
    <br>
    <p class="text-center">Donations help me keep the website in tip-top shape! I appreciate any and all donations. Thank you very much!</p>
    <br>

@endsection