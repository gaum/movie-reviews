@extends('layouts.layout')

@section('title')
    User Page
@endsection

@section('content')
    <div class="row pb-2">
        <div class="col p-2 m-2">
            <!--Only show current user does not own this page-->
            @if ($id != Auth::id())
                <h6 class="card-title">{{ $name }}</h6>
                <p><span class="badge badge-primary">Privilege Level: {{ $role }}</span></p>
                <p><span class="badge badge-info">Rating: {{ $score }}%</span> ({{ $total }} ratings)</p>

                @if ($user)
                    <a href="{{ $user->website }}" target="_blank" class="card-link">{{ $user->website }}</a>
                    <br><br>
                    <p class="card-text">{{ $user->bio }}</p>
                @endif

                <!--Interest already exists-->
                @auth
                    @if ($interest)
                    <div>
                        <form method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group">
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success {{ ($interest->interest=="1")? "active" : "" }}"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit" name="interest" value="0" class="btn btn-outline-danger {{ ($interest->interest=="0")? "active" : "" }}"><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </form>
                    </div>
                    @else
                    <!--Interest noes not-->
                    <div>
                        <form method="POST">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <button type="submit" name="interest" value="1" class="btn btn-outline-success"><i class="far fa-thumbs-up"></i></button>
                                <button type="submit"  name="interest" value="0" class="btn btn-outline-danger"><i class="far fa-thumbs-down"></i></button>
                            </div>
                        </form>
                    </div>
                    @endif
                @else
                    <div class="alert alert-info pb-0">
                        <p>Please log in to leave a user review.</p>
                    </div>
                @endauth
            <!--Current user owns page-->
            @else
                <h6 class="card-title">{{ $name }}</h6>
                <p><span class="badge badge-primary">Privilege Level: {{ $role }}</span></p>
                <p><span class="badge badge-info">Rating: {{ $score }}%</span> ({{ $total }} ratings)</p>

                <!--User already has a bio-->
                @if ($user)
                <div>
                    <form method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="website">Website:</label>
                            <input type="text" maxlength="250" class="form-control" name="website" value="{{ $user->website }}"/>
                        </div>
                        <div class="form-group">
                            <label for="bio">Bio:</label>
                            <textarea class="form-control" maxlength="65000" name="bio" rows="4">{{ $user->bio }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="wiltlevel">Wilted Review Tolerance:</label>
                            <input class="form-control" type="number" name="wiltlevel" min="0" value="{{ $user->wiltlevel }}">
                        </div>
                        <button type="submit" class="btn btn-primary">Update Bio</button>
                    </form>
                </div>
                @else
                <!--User does not have a bio yet-->
                <div>
                    <form method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="review">Website:</label>
                            <input type="text" maxlength="250" class="form-control" name="website"/>
                        </div>
                        <div class="form-group">
                            <label for="review">Bio:</label>
                            <textarea class="form-control" maxlength="65000" name="bio" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="wiltlevel">Wilted Review Tolerance:</label>
                            <input class="form-control" type="number" name="wiltlevel" min="0" value="10">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit Bio</button>
                    </form>
                </div>
                @endif

                <!--User interactions-->
                <div class="row pb-2">
                    <div class="col p-2 m-2">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-reviews-tab" data-toggle="tab" href="#nav-reviews" role="tab" aria-controls="nav-reviews" aria-selected="true">Reviews</a>
                                <a class="nav-item nav-link" id="nav-movie-tab" data-toggle="tab" href="#nav-movie" role="tab" aria-controls="nav-movie" aria-selected="false">Movie Interests</a>
                                <a class="nav-item nav-link" id="nav-actor-tab" data-toggle="tab" href="#nav-actor" role="tab" aria-controls="nav-actor" aria-selected="false">Actor Interests</a>
                                <a class="nav-item nav-link" id="nav-company-tab" data-toggle="tab" href="#nav-company" role="tab" aria-controls="nav-company" aria-selected="false">Company Interests</a>
                                <a class="nav-item nav-link" id="nav-user-tab" data-toggle="tab" href="#nav-user" role="tab" aria-controls="nav-user" aria-selected="false">Reviewer Interests</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-reviews" role="tabpanel" aria-labelledby="nav-home-tab">
                                <ul class="list-group mt-2">
                                    @foreach($moviereviews as $moviereview)
                                        <li class="list-group-item list-group-item-action">
                                            <a href="/movie/{{ $moviereview->movieID }}/1">
                                            <!--Full Stars-->
                                            @for ($i = 1; $i <= $moviereview->score; $i++)
                                                <i class="fas fa-star text-warning"></i>
                                            @endfor
                                            <!--Empty Stars-->
                                            @for ($i = 5; $i > $moviereview->score; $i--)
                                                <i class="far fa-star text-warning"></i>
                                            @endfor
                                            {{ $moviereview->movieName }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="nav-movie" role="tabpanel" aria-labelledby="nav-movie-tab">
                                <ul class="list-group mt-2">
                                    @foreach($movieinterests as $movieinterest)
                                        <li class="list-group-item list-group-item-action">
                                            <a href="/movie/{{ $movieinterest->movieID }}/1">
                                            @if($movieinterest->interest)
                                                <button type="button" name="interest" value="1" class="btn btn-success btn-lg"><i class="far fa-thumbs-up"></i></button>                                
                                            @else
                                                <button type="button" name="interest" value="0" class="btn btn-danger btn-lg"><i class="far fa-thumbs-down"></i></button>
                                            @endif
                                            {{ $movieinterest->movieName }}                                                
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="nav-actor" role="tabpanel" aria-labelledby="nav-actor-tab">
                                <ul class="list-group mt-2">
                                    @foreach($actorinterests as $actorinterest)
                                        <li class="list-group-item list-group-item-action">
                                            <a href="/actor/{{ $actorinterest->actorID }}">
                                            @if($actorinterest->interest)
                                                <button type="button" name="interest" value="1" class="btn btn-success btn-lg"><i class="far fa-thumbs-up"></i></button>                                
                                            @else
                                                <button type="button" name="interest" value="0" class="btn btn-danger btn-lg"><i class="far fa-thumbs-down"></i></button>
                                            @endif
                                            {{ $actorinterest->actorName }}                                                
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="nav-company" role="tabpanel" aria-labelledby="nav-company-tab">
                                <ul class="list-group mt-2">
                                    @foreach($companyinterests as $companyinterest)
                                        <li class="list-group-item list-group-item-action">
                                            <a href="/company/{{ $companyinterest->companyID }}">
                                            @if($companyinterest->interest)
                                                <button type="button" name="interest" value="1" class="btn btn-success btn-lg"><i class="far fa-thumbs-up"></i></button>                                
                                            @else
                                                <button type="button" name="interest" value="0" class="btn btn-danger btn-lg"><i class="far fa-thumbs-down"></i></button>
                                            @endif
                                            {{ $companyinterest->companyName }}                                                
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab">
                                <ul class="list-group mt-2">
                                    @foreach($userinterests as $userinterest)
                                        <li class="list-group-item list-group-item-action">
                                            <a href="/user/{{ $userinterest->id }}">
                                            @if($userinterest->interest)
                                                <button type="button" name="interest" value="1" class="btn btn-success btn-lg"><i class="far fa-thumbs-up"></i></button>                                
                                            @else
                                                <button type="button" name="interest" value="0" class="btn btn-danger btn-lg"><i class="far fa-thumbs-down"></i></button>
                                            @endif
                                            {{ $userinterest->name }}                                           
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection