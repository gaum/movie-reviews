@extends('layouts.layout')

@section('title')
    Movie
@endsection

@section('content')
    <div class="row pb-2">
        <div class="col-12 col-md-3">
            <img class="img-thumbnail mb-2" :src="'https://image.tmdb.org/t/p/w500/'+ movie.poster_path" :alt="movie.title">
            
            <!--Only show interest if movie has not been released-->
            @auth
                <div v-if="new Date(movie.release_date) > Date.now()">
                    <!--Interest already exists-->
                    @if ($interest)
                    <div>
                        <form method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group">
                                <div class="btn-group d-flex" role="group">
                                    <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg {{ ($interest->interest=="1")? "active" : "" }}"><i class="far fa-thumbs-up"></i></button>
                                    <button type="submit" name="interest" value="0" class="btn btn-outline-danger btn-lg {{ ($interest->interest=="0")? "active" : "" }}"><i class="far fa-thumbs-down"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @else
                    <!--Interest noes not exist-->
                    <div class="text-center">
                        <strong>Interested in this movie?</strong>
                        <form method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="movieName" :value="movie.title">

                            <div class="form-group">
                                <div class="btn-group d-flex" role="group" style=>
                                    <button type="submit" name="interest" value="1" class="btn btn-outline-success btn-lg" title="I'm interested!"><i class="far fa-thumbs-up"></i></button>
                                    <button type="submit"  name="interest" value="0" class="btn btn-outline-danger btn-lg" title="I'll give this a skip..."><i class="far fa-thumbs-down"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
                <div v-else>
                    <a class="btn btn-block btn-danger" href="#reviewscan">Review this Movie</a>
                </div>
            @else
                <div class="alert alert-info pb-0">
                    <p>Please log in to leave a user review.</p>
                </div>
            @endauth

        </div>
        <div class="col">

            <h5 class="style card-title font-weight-bold mt-2">@{{ movie.title }}</h5>
            <p><span class="badge badge-primary">Release Date: @{{ movie.release_date }}</span></p>
            <p><span class="badge badge-info">Interest: {{ $interestscore }}%</span> ({{ $totalinterested }} ratings)</p>
            <p class="text-warning">
                <!--Full Stars-->
                @for ($i = 1; $i <= $reviewscore; $i++)
                    <i class="fas fa-star"></i>
                @endfor
                <!--Half Stars (if decimal is greater than or equal to 0.4)-->
                @if(round($reviewscore, 2) - round(floor($reviewscore), 2) >= 0.4)
                    <i class="fas fa-star-half-alt"></i>
                @endif
                <!--Empty stars-->
                @if(round($reviewscore, 2) - round(floor($reviewscore), 2) >= 0.4)
                    @for ($i = 5; $i > $reviewscore+1; $i--)
                        <i class="far fa-star"></i>
                    @endfor
                @else
                    @for ($i = 5; $i > $reviewscore; $i--)
                        <i class="far fa-star"></i>
                    @endfor
                @endif
                <span class="text-dark">({{ $reviewtotal }} reviews)</span>
            <p>

            <p class="card-text">@{{ movie.overview }}</p>

            <p><strong>Runtime:</strong> @{{ movie.runtime }} minutes</p>

            <p><strong>TMDb Score:</strong> @{{ movie.vote_average }}/10</p>
            
            <div class="row">
                <div class="col">
                <strong>Genres:</strong>
                    <span v-for="genre in movie.genres" class="badge badge-primary badge-pill">@{{ genre.name }}&nbsp;</span>
                </div>
            </div>

            <p class="font-weight-bold pt-3">Top Cast:</p>
            <div class="row">
                <div v-for="person in cast.cast.slice(0,6)" class="col-4 col-md-2">
                    <a :href="'/actor/'+ person.id">
                    <img :src="'https://image.tmdb.org/t/p/w500/'+ person.profile_path" width="100%" :alt="person.name"/>
                    @{{ person.name }} as @{{ person.character }}
                    </a>
                </div>
            </div>

            <p class="font-weight-bold pt-3">Photos:</p>
            <div class="row">
                <div v-for="backdrop in images.backdrops.slice(0,30)" class="col-4 col-md-2 pb-2">
                    <a :href="'https://image.tmdb.org/t/p/w500/'+ backdrop.file_path" target="_blank"><img :src="'https://image.tmdb.org/t/p/w500/'+ backdrop.file_path" width="100%"/></a>
                </div>
            </div>

            <p class="font-weight-bold pt-3">Videos:</p>
            <div class="row">
                <div v-for="backdrop in videos.results.slice(0,6)" class="col">
                    <iframe :src="'https://www.youtube-nocookie.com/embed/'+ backdrop.key" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <p class="font-weight-bold pt-3">Production Companies:</p>
            <div class="row">
                <div v-for="production_company in movie.production_companies.slice(0,6)" class="col col-4 col-md-2 pb-2">
                    <a :href="'/company/'+ production_company.id">
                    <img :src="'https://image.tmdb.org/t/p/w500/'+ production_company.logo_path" width="100%" :alt="production_company.name"/>
                    @{{ production_company.name }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row pb-2">
        <div class="col">
            <hr>
        </div>
    </div>

    <div class="row pb-2">
        <div class="col">
            <!--Only show interest if movie has not been released-->
            <div v-if="new Date(movie.release_date) <= Date.now()">
                <h5 class="style font-weight-bold">Personal Review</h5>
                <!--Review already exists-->
                @if ($review)
                <div id="reviewscan">
                    <form method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <input type="hidden" name="movieName" :value="movie.title">

                        <div class="star-rating">
                            <fieldset>
                                <input type="radio" id="star5" name="score" value="5" {{ ($review->score=="5")? "checked" : "" }} /><label for="star5" title="Outstanding"></label>
                                <input type="radio" id="star4" name="score" value="4" {{ ($review->score=="4")? "checked" : "" }} /><label for="star4" title="Very Good"></label>
                                <input type="radio" id="star3" name="score" value="3" {{ ($review->score=="3")? "checked" : "" }} /><label for="star3" title="Good"></label>
                                <input type="radio" id="star2" name="score" value="2" {{ ($review->score=="2")? "checked" : "" }} /><label for="star2" title="Poor"></label>
                                <input type="radio" id="star1" name="score" value="1" {{ ($review->score=="1")? "checked" : "" }}/><label for="star1" title="Very Poor"></label>
                            </fieldset>
                        </div>
                        <div class="form-group">
                            <label for="review">Review Title:</label>
                            <input type="text" maxlength="250" class="form-control" name="title" value="{{ $review->title }}"/>
                        </div>
                        <div class="form-group">
                            <label for="review">Write your personal review:</label>
                            <textarea class="form-control" maxlength="65000" name="review" rows="4">{{ $review->review }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update Review</button>
                    </form>
                </div>
                @else
                <!--Review does not exist-->
                    @auth
                    <div id="reviewscan">
                        <form method="POST">
                            {{ csrf_field() }}

                            <input type="hidden" name="movieName" :value="movie.title">

                            <div class="star-rating">
                                <fieldset>
                                    <input type="radio" id="star5" name="score" value="5" /><label for="star5" title="Outstanding"></label>
                                    <input type="radio" id="star4" name="score" value="4" /><label for="star4" title="Very Good"></label>
                                    <input type="radio" id="star3" name="score" value="3" /><label for="star3" title="Good"></label>
                                    <input type="radio" id="star2" name="score" value="2" /><label for="star2" title="Poor"></label>
                                    <input type="radio" id="star1" name="score" value="1" /><label for="star1" title="Very Poor"></label>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <label for="review">Review Title:</label>
                                <input type="text" maxlength="250" class="form-control" name="title"/>
                            </div>
                            <div class="form-group">
                                <label for="review">Write your personal review:</label>
                                <textarea class="form-control" maxlength="65000" name="review" rows="4"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit Review</button>
                        </form>
                    </div>
                    @else
                        <div id="reviewscan" class="alert alert-info pb-0">
                            <p>Please log in to leave a review.</p>
                        </div>
                    @endauth
                @endif
            </div>
        </div>
    </div>

    <div class="row pb-2">
        <div class="col">
            <hr>
        </div>
    </div>

    <!-- Wilted Modal -->
    @auth
    <div class="modal fade" id="wiltedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Wilted Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="/wilt">
                {{ csrf_field() }}
                <input id="wilted" name="reviewID" type="hidden" value="6">
                <input name="movieID" type="hidden" :value="movie.id">
                <div class="modal-body">
                    <h5>Please select why you marked this review as wilted:<h5>
                    <select name="wiltedType" class="custom-select">
                        <option value="1" selected>I just don't like it</option>
                        <option value="2">Spam</option>
                        <option value="3">Hateful or abusive</option>
                        <option value="4">Sexual content</option>
                        <option value="5">Harmful dangerous acts</option>
                        <option value="6">Other</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn" style="background-color:olive; color:white">Submit</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    @endauth

    <!--Reviews-->
    <div class="row pb-2">

        @foreach ($allreviews as $review)
            <div class="col-12 col-md-6 mb-2">
                <div class="border rounded p-2">
                    @auth
                        @if(Auth::user()->id != $review->userID)
                            <button data-id="{{ $review->reviewID }}" class="btn btn-success float-right wilted" data-toggle="modal" data-target="#wiltedModal" style="background-color:olive" title="I don't like this" type="submit"><i class="fas fa-leaf fa-rotate-90"></i></button>
                        @endif
                    @endauth
                    @if($review->wiltedness < $wiltlevel)
                        <h5 class="card-title font-weight-bold">{{ $review->title }}</h5>
                    @endif
                    <p><span class="badge badge-primary">{{ $review->created_at }}</span></p>
                    <p>Reviewer: <a href="/user/{{ $review->id }}">{{ $review->name }}</a></p>
                    <div name="reviewscore" class="text-warning">
                        <!--Full Stars-->
                        @for ($i = 1; $i <= $review->score; $i++)
                            <i class="fas fa-star"></i>
                        @endfor
                        <!--Empty Stars-->
                        @for ($i = 5; $i > $review->score; $i--)
                            <i class="far fa-star"></i>
                        @endfor
                    </div>
                    @if($review->wiltedness < $wiltlevel)
                        <p class="card-text">{{ $review->review }}</p>
                    @endif
                </div>
            </div>
      
        @endforeach
        <!--Pagination-->
        @if($allreviews->nextPageUrl() > 10)
        <div class="col-12 mt-2">
            <ul class="pagination">
                @if($page == 1)
                    <li class="page-item disabled">
                        <span class="page-link">Previous Page</span>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" :href="'/movie/'+ movie.id +'/{{ $page - 1 }}'">Previous Page</a>
                    </li>
                @endif
                @if($allreviews->currentPage() == $allreviews->lastPage() )
                    <li class="page-item disabled">
                        <span class="page-link">Next Page</span>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" :href="'/movie/'+ movie.id '/{{ $page + 1 }}'">Next Page</a>
                    </li>
                @endif
            </ul>
        </div>
        @endif
    </div>
@endsection

@section('specificJS')
    <!--This goes into the content specificJS of layout.blade.php-->
    <!--Used when you have a JS file only needed for this single view.-->
    <script>
        $( ".wilted" ).click(function() {
            var reviewID = $(this).data('id');
            $("#wilted").val(reviewID);
        });
    </script>

        <script>
        new Vue({
            el: '#app',
            data () {
                return {
                    movie: null,
                    cast: null,
                    images: null,
                    videos: null
                }
            },
            mounted () {          
                //Movie
                axios
                .get('https://api.themoviedb.org/3/movie/'+{{ $id }}+'?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.movie = response.data)).then(response => (document.title = this.movie.title))              

                //Cast
                axios
                .get('https://api.themoviedb.org/3/movie/'+{{ $id }}+'/credits?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.cast = response.data))

                //Images
                axios
                .get('https://api.themoviedb.org/3/movie/'+{{ $id }}+'/images?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.images = response.data))

                //Videos
                axios
                .get('https://api.themoviedb.org/3/movie/'+{{ $id }}+'/videos?api_key=beb0c664b4b2eee983a943078de43bdd')
                .then(response => (this.videos = response.data))

            }
        })
    </script>
@endsection