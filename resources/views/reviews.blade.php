@extends('layouts.layout')

@section('title')
    Reviews
@endsection

@section('content')
    <div class="row mt-4">
        <div class="col-12 col-md-4  pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style mt-1 pt-1 pb-1 text-white">Recent Reviews</h3>
            </div>
            <div class="list-group">
            @foreach ($reviews as $review)                            
                <li class="list-group-item list-group-item-action"><a href="/movie/{{ $review->movieID }}/1">{{ $review->name }} reviewed {{ $review->movieName }}</a></li> 
            @endforeach
            </div>
        </div>
        <div class="col-12 col-md-4 pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style mt-1 pt-1 pb-1 text-white">Top Reviewed Movies</h3>
            </div>
            <div class="list-group">
            @foreach ($movies as $movie)  
                <li class="list-group-item list-group-item-action"><a href="/movie/{{ $movie->movieID }}/1">{{ $movie->movieName }} - {{ $movie->count }} Reviews</a></li>                
            @endforeach
            </div>
        </div>
        <div class="col-12 col-md-4 pr-2 pl-2">
            <div class="links bg-primary rounded text-center">
                <h3 class="style mt-1 pt-1 pb-1 text-white">Top Reviewers<h3>
            </div>
            <div class="list-group">
            @foreach ($reviewers as $reviewer)                            
                <li class="list-group-item list-group-item-action"><a href="/user/{{ $reviewer->userID }}">{{ $reviewer->name }} - {{ $reviewer->count }} Reviews</a></li> 
            @endforeach
            </div>
        </div>
    </div>
                
@endsection
