<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class movies extends Controller
{
    public function popular($page)
    {
        return view('movies', [
            'page' => $page,
            'searchType' => 'popular'
        ]);
    }

    public function top($page)
    {
        return view('movies', [
            'page' => $page,
            'searchType' => 'top'
        ]);
    }

    public function upcoming($page)
    {
        return view('movies', [
            'page' => $page,
            'searchType' => 'upcoming'
        ]);
    }

    public function playing($page)
    {
        return view('movies', [
            'page' => $page,
            'searchType' => 'playing'
        ]);
    }
}
