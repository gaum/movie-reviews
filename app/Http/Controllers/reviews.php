<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MovieReview;

class reviews extends Controller
{
    public function index()
    {
        //Get review info
        $reviews = MovieReview::selectRaw('moviereviews.id, moviereviews.movieID, moviereviews.movieName, users.name')
                                ->join('users', 'users.id', '=', 'moviereviews.userID')
                                ->orderBy('id', 'desc')
                                ->take(10)->get();

        //Get reviewer info
        $reviewers = MovieReview::selectRaw('moviereviews.userID, users.name, count(moviereviews.userID) as count')
                                    ->join('users', 'users.id', '=', 'moviereviews.userID')
                                    ->groupBy('users.name', 'moviereviews.userID')
                                    ->orderBy('count', 'desc')
                                    ->take(10)->get();

        //Get review info
        $movies = MovieReview::selectRaw('movieID, movieName, count(movieID) as count')
                                ->groupBy('movieName', 'movieID')
                                ->orderBy('count', 'desc')
                                ->take(10)->get();

        //dd($movies);

        return view('reviews', [
            'reviews' => $reviews,
            'reviewers' => $reviewers,
            'movies' => $movies
        ]);
    }
}
