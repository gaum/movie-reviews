<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\MovieReview;
use App\MovieInterest;
use App\UserInfo;
use App\Wilted;

class movie extends Controller
{
    public function index($id, $page)
    {
        $userID = Auth::id();    

        //Get review info
        $review = MovieReview::where('movieID', $id)->where('userID', $userID)->first();

        //Get interest info
        $interest = MovieInterest::where('movieID', $id)->where('userID', $userID)->first();

        //Get movie interest score
        $interested = MovieInterest::where('movieID', $id)
                                    ->whereIn('interest', ['1'])->count();        
        $totalinterested = MovieInterest::where('movieID', $id)->count();

        if($totalinterested == 0)
            $interestscore = 0;
        else
            $interestscore = $interested / $totalinterested * 100;

        //Get movie review score
        $reviewscore = MovieReview::where('movieID', $id)
                                    ->avg('score');  
        
        //Get movie review score
        $reviewtotal = MovieReview::where('movieID', $id)
                                    ->count('movieID'); 
                                    
        //Get review info
        $allreviews = MovieReview::selectRaw('SUM(wilted.wiltedness) AS wiltedness,moviereviews.id AS reviewID,moviereviews.userID,users.id,moviereviews.title,moviereviews.created_at,moviereviews.score,users.name,moviereviews.review')
                                    ->join('users', 'moviereviews.userID', '=', 'users.id')
                                    ->leftJoin('wilted', 'moviereviews.id', '=', 'wilted.reviewID')
                                    ->where('movieID', $id)
                                    ->groupBy('moviereviews.id', 'moviereviews.userID', 'users.id', 'moviereviews.title', 'moviereviews.created_at','moviereviews.score','users.name','moviereviews.review')
                                    ->paginate(10);

        //Wilted + Public Wilted
        $wiltlevel = UserInfo::select('wiltlevel')->where('userID', $userID)->first();   
        if(!$wiltlevel)
        {
            $wiltlevel = new \stdClass();
            $wiltlevel->wiltlevel = '10';
        }            

        return view('movie', [
            'id' => $id,
            'review' => $review,
            'interest' => $interest,
            'totalinterested' => $totalinterested,
            'interestscore' => $interestscore,
            'reviewscore' => $reviewscore,
            'reviewtotal' => $reviewtotal,
            'allreviews' => $allreviews,
            'page' => $page,
            'wiltlevel' => $wiltlevel->wiltlevel
        ]);
    }

    public function store()
    {
        $userID = Auth::id();

        if(request('interest') != null)
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'interest' => 'required|boolean',
                    'movieName' => 'nullable|string',
                ]
            ); 

            MovieInterest::create([
                'movieID' => request()->segment(2),
                'userID' => $userID,
                'interest' => request('interest'),
                'movieName' => request('movieName'),
            ]);
        }
        else if(request('score'))
        {
            //Data validation using specific rules.
                $this->validate(request(), 
                [
                    'score' => 'required|numeric|gte:1|lte:5',
                    'movieName' => 'nullable|string',
                    'title' => 'nullable|string',
                    'review' => 'nullable|string'
                ]
            ); 

            MovieReview::create([
                'movieID' => request()->segment(2),
                'userID' => $userID,
                'movieName' => request('movieName'),
                'score' => request('score'),
                'title' => request('title'),
                'review' => request('review'),
            ]);
        }

        return redirect('/movie/'.request()->segment(2).'/1')->with('success', 'Movie has been rated.');
    }

    public function update()
    {
        $userID = Auth::id();

        if(request('interest') != null)
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'interest' => 'required|boolean',
                ]
            ); 

            MovieInterest::where('movieID', request()->segment(2))
                            ->where('userID', $userID)
                            ->update([
                                'interest' => request('interest'),
                            ]);
        }
        else if(request('score'))
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'score' => 'required|numeric|gte:1|lte:5',
                    'movieName' => 'nullable|string',
                    'title' => 'nullable|string',
                    'review' => 'nullable|string'
                ]
            ); 

            MovieReview::where('movieID', request()->segment(2))
                        ->where('userID', $userID)
                        ->update([
                            'score' => request('score'),
                            'title' => request('title'),
                            'review' => request('review'),
                            'movieName' => request('movieName'),
                        ]);
        }

        return redirect('/movie/'.request()->segment(2).'/1')->with('success', 'Movie rating updated.');        
    }

    public function destroy()
    {
        $userID = Auth::id();
        
    }
}
