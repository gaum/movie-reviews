<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CompanyReview;

class companypage extends Controller
{
    public function index($id)
    {
        $userID = Auth::id();

        //Get interest info
        $interest = CompanyReview::where('companyID',$id)->where('userID', $userID)->first();

        //Get user interest score
        $interested = CompanyReview::where('companyID', $id)
                                ->whereIn('interest', ['1'])->count();        
        $total = CompanyReview::where('companyID', $id)->count();

        if($total == 0)
            $score = 0;
        else
            $score = $interested / $total * 100;

        return view('companypage', [
            'id' => $id,
            'interest' => $interest,
            'score' => $score,
            'total' => $total
        ]);
    }

    public function store()
    {
        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'interest' => 'required|boolean',
                'companyName' => 'nullable|string',
            ]
        );

        CompanyReview::create([
            'companyID' => request()->segment(2),
            'userID' => Auth::id(),
            'interest' => request('interest'),
            'companyName' => request('companyName'),
        ]);

        return redirect('/company/'.request()->segment(2));
    }

    public function update()
    {
        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'interest' => 'required|boolean',
            ]
        );
        
        CompanyReview::where('companyID', request()->segment(2))
                        ->where('userID', Auth::id())
                        ->update([
                            'interest' => request('interest'),
                        ]);

        return redirect('/company/'.request()->segment(2));        
    }

    public function destroy()
    {
        
    }
}
