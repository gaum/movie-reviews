<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ActorReview;

class actorpage extends Controller
{
    public function index($id)
    {
        $userID = Auth::id();

        //Get interest info
        $interest = ActorReview::where('actorID',$id)->where('userID', $userID)->first();

        //Get user interest score
        $interested = ActorReview::where('actorID', $id)
                                ->whereIn('interest', ['1'])->count();        
        $total = ActorReview::where('actorID', $id)->count();

        if($total == 0)
            $score = 0;
        else
            $score = $interested / $total * 100;

        return view('actorpage', [
            'id' => $id,
            'interest' => $interest,
            'score' => $score,
            'total' => $total
        ]);
    }

    public function store()
    {
        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'interest' => 'required|boolean',
                'actorName' => 'nullable|string',
            ]
        ); 

        ActorReview::create([
            'actorID' => request()->segment(2),
            'userID' => Auth::id(),
            'interest' => request('interest'),
            'actorName' => request('actorName'),
        ]);

        return redirect('/actor/'.request()->segment(2));
    }

    public function update()
    {
        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'interest' => 'required|boolean',
            ]
        ); 
        
        ActorReview::where('actorID', request()->segment(2))
                        ->where('userID', Auth::id())
                        ->update([
                            'interest' => request('interest'),
                        ]);

        return redirect('/actor/'.request()->segment(2));        
    }

    public function destroy()
    {
        
    }
}
