<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UserInfo;
use App\User;
use App\UserReview;
use App\MovieReview;
use App\MovieInterest;
use App\ActorReview;
use App\CompanyReview;

class userpage extends Controller
{
    public function index($id)
    {
        $userID = Auth::id();

        //Get user info
        $user = UserInfo::where('userID', $id)->first();
        $userRole = User::select('roles.name as role')
                            ->where('users.id', $id)
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')->first();

        //Get extended user info
        $user_extended = User::select('users.id as id','users.name as name')
                                ->where('id', $id)->first();

        //Get interest info
        $interest = UserReview::where('userID',$id)->where('reviewerID', $userID)->first();

        //Get user interest score
        $interested = UserReview::where('userID', $id)
                                ->whereIn('interest', ['1'])->count();        
        $total = UserReview::where('userID', $id)->count();

        if($total == 0)
            $score = 0;
        else
            $score = $interested / $total * 100;

        //Get user movie reviews
        $moviereviews = MovieReview::where('userID', $userID)->orderBy('id', 'desc')->get();

        //Get user movie interests
        $movieinterests = MovieInterest::where('userID', $userID)->orderBy('id', 'desc')->get();

        //Get user actor interests
        $actorinterests = ActorReview::where('userID', $userID)->orderBy('id', 'desc')->get();

        //Get user company interests
        $companyinterests = CompanyReview::where('userID', $userID)->orderBy('id', 'desc')->get();

        //Get user user interests
        $userinterests = UserReview::select('users.id', 'users.name', 'userreviews.interest')
                        ->where('reviewerID', $userID)
                        ->join('users', 'userreviews.userID', '=', 'users.id')
                        ->orderBy('userreviews.id', 'desc')
                        ->get();

        return view('userpage', [
            'user' => $user,
            'interest' => $interest,
            'id' => $user_extended->id,
            'name' => $user_extended->name,
            'role' => $userRole->name,
            'score' => $score,
            'total' => $total,
            'moviereviews' => $moviereviews,
            'movieinterests' => $movieinterests,
            'actorinterests' => $actorinterests,
            'companyinterests' => $companyinterests,
            'userinterests' => $userinterests,
        ]);
    }

    public function store()
    {
        if(request('interest') != null)
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'interest' => 'required|boolean',
                ]
            ); 

            UserReview::create([
                'userID' => request()->segment(2),
                'reviewerID' => Auth::id(),
                'interest' => request('interest'),
            ]);
        }
        else
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'bio' => 'nullable|string',
                    'website' => 'nullable|string',
                    'wiltlevel' => 'required|numeric|gte:0'
                ]
            ); 

            UserInfo::create([
                'userID' => Auth::id(),
                'bio' => request('bio'),
                'website' => request('website'),
                'wiltlevel' => request('wiltlevel')
            ]);
        }

        return redirect('/user/'.request()->segment(2))->with('success', 'Profile successfully updated.');
    }

    public function update()
    {
        if(request('interest') != null)
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'interest' => 'required|boolean',
                ]
            ); 

            UserReview::where('userID', request()->segment(2))
                            ->where('reviewerID', Auth::id())
                            ->update([
                                'interest' => request('interest'),
                            ]);
        }
        else
        {
            //Data validation using specific rules.
            $this->validate(request(), 
                [
                    'bio' => 'nullable|string',
                    'website' => 'nullable|string',
                    'wiltlevel' => 'required|numeric|gte:0'
                ]
            ); 
            
            UserInfo::where('userID', Auth::id())
                        ->update([
                            'bio' => request('bio'),
                            'website' => request('website'),
                            'wiltlevel' => request('wiltlevel')
                        ]);
        }

        return redirect('/user/'.request()->segment(2))->with('success', 'Profile successfully updated.');        
    }

    public function destroy()
    {
        
    }
}
