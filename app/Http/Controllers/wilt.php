<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Wilted;
use App\User;

class wilt extends Controller
{
    public function wilt()
    {
        $userID = Auth::id();

        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'reviewID' => [
                    'required',
                    'numeric',
                    Rule::notIn($userID),
                    Rule::unique('wilted', 'reviewID')->where(function ($query) {
                        $userID = Auth::id();
                        return $query->where('userID', '=', $userID);
                    })
                ],
                'wiltedType' => 'required|numeric|gte:1|lte:6',
            ],
            [
                'reviewID.not_in' => 'You have already wilted this review.',
                'reviewID.unique' => 'You have already wilted this review.'
            ]
        );    
        
        $userRole = Auth::user()->getRole();

        //Admins get 10, moderators 5, other 1
        if($userRole->name == 'Administrator')
            $wiltedness = 10;
        elseif($userRole->name == 'Moderator')
            $wiltedness = 5;
        else
            $wiltedness = 1;
    
        Wilted::create([
            'reviewID' => request('reviewID'),
            'userID' => $userID,
            'wiltedType' => request('wiltedType'),
            'wiltedness' => $wiltedness,
        ]);

        return redirect('/movie/'.request('movieID').'/1')->with('success', 'Review has been wilted.');
    }
}
