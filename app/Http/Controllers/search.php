<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserInfo;
use App\User;
use App\UserReview;
use Illuminate\Validation\Rule;

class search extends Controller
{
    public function index($page)
    {
        $page = 1;

        //Data validation using specific rules.
        $this->validate(request(), 
            [
                'search' => 'required|string',
                'searchtype' => [
                    'required',
                    Rule::in(['movie', 'actor', 'user', 'company', 'actormovie']),
                ],
            ]
        ); 

        $searchvalue = request('search');
        $searchtype = request('searchtype');

        if($searchtype == 'user')
        {
            //Get user info
            $results = User::select('users.id as id','users.name as name', 'roles.name as role', 'userinfo.bio as bio', 'userinfo.website as website')
                                ->where('users.name', 'LIKE', '%'.strtolower($searchvalue).'%')
                                ->join('userinfo', 'users.id', '=', 'userinfo.userID')
                                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                                ->join('roles', 'role_user.role_id', '=', 'roles.id')->take(20)->get();  
        }
        else
        {
            $results = null;
        }

        return view('search', [
            'results' => $results,
            'page' => $page,
            'searchtype' => $searchtype,
            'searchvalue' => $searchvalue
        ]);
    }

    public function search($searchtype, $page, $search)
    {
        if($searchtype == 'user')
        {
            //Get user info
            $results = User::select('users.id as id','users.name as name', 'roles.name as role', 'userinfo.bio as bio', 'userinfo.website as website')
                                ->where('users.name', 'LIKE', '%'.strtolower($search).'%')
                                ->join('userinfo', 'users.id', '=', 'userinfo.userID')
                                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                                ->join('roles', 'role_user.role_id', '=', 'roles.id')->take(20)->get(); 

            $total_pages = 0;

        }
        else
        {
            $results = null;
        }

        return view('search', [
            'results' => $results,
            'page' => $page,
            'searchtype' => $searchtype,
            'searchvalue' => $search
        ]);
    }
}
