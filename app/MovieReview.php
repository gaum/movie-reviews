<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieReview extends Model
{
    protected $table = 'moviereviews';

    protected $fillable = [
        'movieID',
        'userID',
        'score',
        'title',
        'review',
        'movieName'
    ];
}
