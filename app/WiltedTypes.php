<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WiltedTypes extends Model
{
    protected $table = 'wilted_types';

    protected $fillable = [
        'wiltedID',
        'name',
    ];
}
