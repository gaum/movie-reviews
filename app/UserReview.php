<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    protected $table = 'userreviews';

    protected $fillable = [
        'reviewerID',
        'userID',
        'interest',
    ];
}
