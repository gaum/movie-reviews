<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActorReview extends Model
{
    protected $table = 'actorreviews';

    protected $fillable = [
        'actorID',
        'userID',
        'interest',
        'actorName'
    ];
}
