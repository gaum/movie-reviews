<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wilted extends Model
{
    protected $table = 'wilted';

    protected $fillable = [
        'reviewID',
        'userID',
        'wiltedType',
        'wiltedness'
    ];
}
