<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'userinfo';

    protected $fillable = [
        'userID',
        'bio',
        'website',
        'wiltlevel'
    ];
}
