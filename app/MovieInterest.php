<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieInterest extends Model
{
    protected $table = 'movieinterest';

    protected $fillable = [
        'movieID',
        'userID',
        'interest',
        'movieName'
    ];
}
