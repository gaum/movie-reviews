<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyReview extends Model
{
    protected $table = 'companyreviews';

    protected $fillable = [
        'companyID',
        'userID',
        'interest',
        'companyName'
    ];
}
